const express = require('express');
const projectController = require('./../controllers/projectController');
const router = express.Router();


router.route('/allNoTasks').get(projectController.getAllNoTasks);
router.route('/allTasks').get(projectController.getAllTasks);
router.route('/oneTask').get(projectController.getOneTask);
router.route('/delete').delete(projectController.deleteProject);
router.route('/create').post(projectController.createProject);
router.route('/update/:index').patch(projectController.updateProject);


module.exports = router;

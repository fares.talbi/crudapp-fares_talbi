const fs = require('fs'); 
const projects = JSON.parse(fs.readFileSync(`${__dirname}/../data/data-source.json`));


//Get projects that have no tasks

exports.getAllNoTasks = (req, res) => {
    const noTasksProjects = projects.filter(el => !el.tasks);
    res.status(200).json({
      status: 'success',
      data: {
        noTasksProjects,
      },
    });
  };

  //Get projects that have tasks
  exports.getAllTasks = (req, res) => {
   const tasksProjects = projects.filter(el => el.tasks);
    res.status(200).json({
      status: 'success',
      data: {
        tasksProjects,
      },
    });
  };

//Get One project That have tasks
exports.getOneTask = (req, res) => {
    const oneTask = projects.find(el => el.tasks);
     res.status(200).json({
       status: 'success',
       data: {
         oneTask,
       },
     });
   };

   //Delete project that have the status done
    exports.deleteProject = (req, res) => {
    const newProjects = projects.filter(el => el.status !== "done");
        res.status(200).json({
            status: 'success',
            data: {
              newProjects,
            },
          });

   };

    //Create project
   exports.createProject = (req, res) => {
    const newCreatedProject = req.body;
     projects.push(newCreatedProject);
   
        res.status(201).json({
            status: 'success',
            data: {
                newCreatedProject,
            }
        })
  };

  //Update Project
  exports.updateProject = (req, res) => {
    const index = req.params.index;
    const task1 = req.body.task1;
    const task2 = req.body.task2;
    const task3 = req.body.task3;
    const arrayTasks = [task1, task2, task3];
    if(projects[index].tasks){
        projects[index].tasks.push(task1, task2, task3);
    } else {
        Object.assign(projects[index], {"tasks" :arrayTasks});
    }
        res.status(200).json({
            status: 'success',
            data: {
                projects,
            }
        })
  };


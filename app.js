const express = require('express');
const app = express();
const projectRoutes = require('./routes/projectRoutes');

app.use(express.json());

app.use('/api/projects', projectRoutes);

const port = 8000;
app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});